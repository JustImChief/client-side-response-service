import { AxiosPromise, AxiosResponse } from 'axios';

declare interface ResponseServiceError extends Error {
}

declare interface ResponseService {
}

declare class ResponseService {
  doFailureAction<T = any>(error: T): Promise<T>;

  doSuccessAction<T = any>(response: AxiosResponse): Promise<AxiosResponse<T>>;

  processResponse(promise: AxiosPromise): Promise<AxiosResponse>;
}

export default ResponseService;
export { ResponseServiceError };