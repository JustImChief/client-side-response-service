import getReasonMessage from '@jclib/http-statuses';

import ResponseServiceError from './ResponseServiceError';

class ResponseService {
  doFailureAction = (err) => {
    return err;
  };

  doSuccessAction = (res) => {
    return res?.data || {};
  };

  /**
   * @throws {ResponseServiceError}
   */
  processResponse = (promise) => {
    return promise
      .then((response) => {
        if (response.status >= 200 && response.status < 300) {
          return response;
        }

        throw ResponseServiceError;
      })
      .catch((error) => {
        if (error?.response?.status) {
          return {
            errorType: 'error',
            message:   getReasonMessage(error.response.status),
          };
        }

        return error;
      });
  };
}

export default ResponseService;