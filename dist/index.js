"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _httpStatuses = _interopRequireDefault(require("@jclib/http-statuses"));

var _ResponseServiceError = _interopRequireDefault(require("./ResponseServiceError"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class ResponseService {
  constructor() {
    _defineProperty(this, "doFailureAction", err => {
      return err;
    });

    _defineProperty(this, "doSuccessAction", res => {
      return (res === null || res === void 0 ? void 0 : res.data) || {};
    });

    _defineProperty(this, "processResponse", promise => {
      return promise.then(response => {
        if (response.status >= 200 && response.status < 300) {
          return response;
        }

        throw _ResponseServiceError.default;
      }).catch(error => {
        var _error$response;

        if (error !== null && error !== void 0 && (_error$response = error.response) !== null && _error$response !== void 0 && _error$response.status) {
          return {
            errorType: 'error',
            message: (0, _httpStatuses.default)(error.response.status)
          };
        }

        return error;
      });
    });
  }

}

var _default = ResponseService;
exports.default = _default;